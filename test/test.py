import os 

os.system("vivado -mode batch -source automate.tcl")

f1 = open("test_in.txt", "r")
f2 = open("output.txt", "r")

a = []
b = []
p = []

print("\n")

for line in f1.readlines():
    ai, bi = line.split()
    a.append(int(ai))
    b.append(int(bi))

for line in f2.readlines():
    p.append(int(line))

p = p[1:]

err = 0

for i in range(len(p)):
    p_exp = a[i] * b[i]
    print('a = {:>4}, b = {:>4},  p = {:>7},  expected p = {:>7}'.format(a[i], b[i], p[i], p_exp))
    if(p[i] != p_exp):
        err+=1

print("\n")

if(err):
    print("FAIL.",err," out of",len(p),"failed.")
else:
    print("SUCCESS. ALl test cases passed.")

