# Assignment - Sequential Assignment

This assignment deals with implementation of a sequential multiplier in Verilog HDL.  


## Details on the assignment

### Multiplication

This basically refers to the "long form" multiplication as we learn in school.  For each digit of the multiplier, we shift the multiplicand to the appropriate place value, and then add all the partial products.  An example of the partial products in binary for a simple 4-bit multiplication are shown below.  As expected, multiplying two 4-bit numbers can result in an output that is up to 8 bits in length.  

```
        0110    # Decimal 6 - Multiplicand
      x 0011    # Decimal 3 - Multiplier
    --------
        0110    # Partial product 0 (PP0)
       0110     # PP1
      0000      # PP2 
     0000       # PP3
    --------
    00010010    # Decimal 18
    --------
```

### Number representation

The numbers themselves are represented in 2's complement notation.  Therefore, if the multiplicand is negative, the PP values should be "sign-extended" to get the correct result.  An example for negative multiplicand is shown below - negative multiplier requires similar careful handling, and is left as an exercise.

```
        1010    # Decimal -6
      x 0011    # Decimal 3
    --------
    11111010    # Partial product 0 (PP0) - sign extended
    1111010     # PP1
    000000      # PP2 
    00000       # PP3
    --------
 (1)11101110    # Decimal -18 (discard the overflow 1)
    --------
```

### Hardware implementation

The above multiplication process can be directly implemented as combinational logic, where each partial product vector is created using a set of AND gates, and the results are put through a chain of adders.

The other alternative is to have a reduced hardware with a single register to hold the final product.  This **accumulates** the final product by iterating over several clock cycles (how many?).  A diagram indicating the architecture is shown here.

![Sequential Multiplier](./fig/seqmult.png)


### Verilog Simulation

Clone this repository to your system. The template code for the sequential multiplier is provided in the file seq-mult.v Once you modify the necessary parts of this file, test the design using the provided testbench - the same way as you did with the inverter in the previous lab session. Note that the provided testbench is a "self-testing" testbench - a few test cases have been provided in it, and a PASS is displayed if all the test cases pass successfully. You are encouraged to modify the test cases in the testbench, and simulate to check for correctness.

To clone this repository, run the following command on the terminal :
```
https://git.ee2003.dev.iitm.ac.in/EE2003-2022/sequential-multiplier-a1.git
```

To compile the files and run the simulations, run the following commands :
```
iverilog seq-mult.v seq-mult-tb.v -o seq-mult
./seq-mult
```
### Testing on FPGA

The sequential multiplier module is to be wrapped inside a block design, interfaced with the VIO blocks. Once the bitstream is generated for the block design, the '.bit' and '.ltx' files(which are located inside <PROJECT_NAME>/<PROJECT_NAME>.runs/impl_1) are to be stored inside the [*test*](./test/) directory. Script to run the "self-checking" VIO tests has been provided. Additional test cases can be added to [*test_in.txt*](./test/test_in.txt)

To run the tests, run the following commands :
```
cd test 
python test.py 
```
